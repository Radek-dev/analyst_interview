import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt
from pandas.api.types import CategoricalDtype
from scipy.stats import poisson
import statsmodels.api as sm

sns.set(rc={'figure.figsize': (11.7, 6.5)})
sns.set_theme(style="darkgrid")
pd.set_option('display.max_columns', None)

DF, df_definitions = pd.DataFrame(), pd.DataFrame()


def create_plots():
    """This function does general plotting for the overall data set."""
    global DF, df_definitions
    # Plot order quantity
    order_qty_series = DF['ORDERED_QTY'][DF['UNIT_OF_MEASURE'] == 'EACH'].value_counts()
    order_qty_series.sort_index(inplace=True)
    df_qty = pd.DataFrame({"frequency": order_qty_series.values, 'qty': order_qty_series.index})
    df_qty.sort_values('qty')
    plt.figure()
    chart = sns.barplot(x=order_qty_series.index, y=order_qty_series.values)
    chart.set_xticklabels(chart.get_xticklabels(), rotation=45, horizontalalignment='right')
    labels = [str(int(v)) if v else '' for v in chart.containers[0].datavalues]
    chart.bar_label(chart.containers[0], labels=labels)
    chart.set(xlabel='Order Quantity', ylabel='Count')
    plt.suptitle(df_definitions[df_definitions['Column'] == 'ORDERED_QTY']['Description'].values[0] +
                 " excluding GRAM quantities", y=0.025)
    plt.show()

    # plot Unit of Measure
    plt.figure()
    chart = sns.histplot(DF['UNIT_OF_MEASURE'])
    labels = [str(int(v)) if v else '' for v in chart.containers[0].datavalues]
    chart.bar_label(chart.containers[0], labels=labels)
    plt.suptitle(df_definitions[df_definitions['Column'] == 'UNIT_OF_MEASURE']['Description'].values[0], y=0.025)
    plt.show()

    # plot Pick_type
    plt.figure()
    chart = sns.histplot(DF['PICK_TYPE'])
    labels = [str(int(v)) if v else '' for v in chart.containers[0].datavalues]
    chart.bar_label(chart.containers[0], labels=labels)
    plt.suptitle(df_definitions[df_definitions['Column'] == 'PICK_TYPE']['Description'].values[0], y=0.025)
    plt.show()

    # Plot QTY
    plt.figure()
    chart = sns.histplot(DF['QTY'])
    labels = [str(int(v)) if v else '' for v in chart.containers[0].datavalues]
    chart.bar_label(chart.containers[0], labels=labels)
    plt.suptitle(df_definitions[df_definitions['Column'] == 'QTY']['Description'].values[0], y=0.025)
    plt.show()

    # Plot PICKED_UNIT_OF_MEASURE
    plt.figure()
    chart = sns.histplot(DF['PICKED_UNIT_OF_MEASURE'])
    labels = [str(int(v)) if v else '' for v in chart.containers[0].datavalues]
    chart.bar_label(chart.containers[0], labels=labels)
    plt.suptitle(df_definitions[df_definitions['Column'] == 'PICKED_UNIT_OF_MEASURE']['Description'].values[0], y=0.025)
    plt.show()

    # Plot PICK_WEIGHT
    plt.figure()
    chart = sns.histplot(DF['PICK_WEIGHT'])
    labels = [str(int(v)) if v else '' for v in chart.containers[0].datavalues]
    chart.bar_label(chart.containers[0], labels=labels)
    plt.suptitle(df_definitions[df_definitions['Column'] == 'PICK_WEIGHT']['Description'].values[0], y=0.025)
    plt.show()

    # Plot TEMPERATURE_CLASS
    plt.figure()
    chart = sns.histplot(DF['TEMPERATURE_CLASS'])
    labels = [str(int(v)) if v else '' for v in chart.containers[0].datavalues]
    chart.bar_label(chart.containers[0], labels=labels)
    plt.suptitle(df_definitions[df_definitions['Column'] == 'TEMPERATURE_CLASS']['Description'].values[0], y=0.025)
    plt.show()

    # Plot event_hour
    plt.figure()
    chart = sns.histplot(DF['event_hour'])
    labels = [str(int(v)) if v else '' for v in chart.containers[0].datavalues]
    chart.bar_label(chart.containers[0], labels=labels)
    plt.suptitle('Event hour based on event_time', y=0.025)
    plt.show()


def run_general_data_analysis():
    """
    This function focus on running general distributional properties fo the data.
    Note that the analysis is done for each column, not subsets from the columns
    are selected.
    """
    global DF, df_definitions
    print("=======Null Check==========")
    print(DF.isnull().sum())
    print("\n=======NA Check==========")
    print(DF.isna().sum())
    print("\n=======Dtypes==========")
    print(DF.dtypes)
    print("\n======= Descriptions =========")
    print(DF[['ORDERED_QTY', 'QTY', 'PICK_WEIGHT', 'event_hour']].describe())
    print("\n")
    print(DF.describe(include='category'))


def time_to_complete_order():
    """
    This function focuses on work with time to pick/collect each order. Some analysis are carried out here.
    """
    global DF
    total_number_of_orders = len(DF['order_number'].unique())
    number_of_hours = len(DF['event_hour'].unique())

    orders_per_hour = total_number_of_orders / number_of_hours
    print(f"average order number per hour: {orders_per_hour}")
    df_length_of_event_max = DF[['event_time', 'order_number']].groupby('order_number')['event_time'].max()
    df_length_of_event_min = DF[['event_time', 'order_number']].groupby('order_number')['event_time'].min()
    df_length_of_event = pd.concat([df_length_of_event_min, df_length_of_event_max], axis=1)
    df_length_of_event.columns = ['event_time_start', 'event_time_end']

    lst_time_delta = []
    for start, end in zip(df_length_of_event['event_time_start'].to_list(),
                          df_length_of_event['event_time_end'].to_list()):
        delta = ((end.hour * 60 + end.minute) - (start.hour * 60 + start.minute))/60
        lst_time_delta.append(delta)

    df_length_of_event['order_time_in_hours'] = lst_time_delta

    average_delta = df_length_of_event['order_time_in_hours'].mean()
    print(f"Average time to collect items on an order: {average_delta}")

    plt.figure()
    chart = sns.histplot(df_length_of_event['order_time_in_hours'])
    labels = [str(int(v)) if v else '' for v in chart.containers[0].datavalues]
    chart.bar_label(chart.containers[0], labels=labels)
    plt.suptitle('Time to complete an order in hours.', y=0.025)
    plt.plot([average_delta, average_delta], [0, 75], color='red')
    plt.show()

    plt.figure()
    sns.displot(df_length_of_event['order_time_in_hours'], kind="ecdf")
    plt.suptitle('Cumulative distribution of order time in hours.', y=0.032)
    plt.show()

    # poisson distribution fit
    df_length_of_event['order_time_in_hours_rounded'] = df_length_of_event['order_time_in_hours'].round()
    X = np.ones_like(df_length_of_event['order_time_in_hours_rounded'])

    fit = sm.Poisson(df_length_of_event['order_time_in_hours_rounded'], X).fit()
    print(fit.summary())
    poisson_lambda = np.exp(fit.params[0])
    print(f"Mean parameter for the Poisson distribution is: {poisson_lambda}")

    x_linspace = np.linspace(0, 8, 100)
    plt.figure()
    chart = sns.histplot(df_length_of_event['order_time_in_hours_rounded'],
                         kde=False, stat="density", label='Real', discrete=True)
    chart.plot(x_linspace,
               poisson.pmf(x_linspace, poisson_lambda), 'g-', lw=2, label='Fitted Poisson')
    chart.legend()
    plt.title("Real vs Fitted Poisson Distribution")
    plt.show()
    prepare_data_for_glm(df_length_of_event)


def prepare_data_for_glm(df_external: pd.DataFrame):
    """
    This function prepares the data for the GLM in R
    The final dataframe is saved to a csv file.
    """
    global DF
    df_counts = DF[['order_number', 'picker_id', 'tote_number', 'ordered_product_id']].groupby('order_number').nunique()
    df_counts.columns = ['picker_id_unique_count', 'tote_number_unique_count', 'ordered_product_id_unique_count']

    df = df_external
    df = pd.concat([df, df_counts], axis=1)

    DF['ORDERED_QTY_NO_GRAMS_SUM'] = np.where(DF['UNIT_OF_MEASURE'] == 'GRAM', 1, DF['ORDERED_QTY'])
    df = pd.concat([df,
                    DF[['order_number', 'ORDERED_QTY_NO_GRAMS_SUM']].groupby('order_number').sum()
                    ], axis=1)

    df_pick_type_normal_only = DF[['order_number', 'PICK_TYPE']].groupby(
        'order_number').apply(lambda df_group: 1 if {"NORMAL"} == set(df_group['PICK_TYPE']) else 0)
    df_pick_type_normal_only.name = "PICK_TYPE_NORMAL_ONLY"

    df = pd.concat([df, df_pick_type_normal_only], axis=1)

    df_pick_weight = DF[['order_number', 'PICK_WEIGHT']].groupby(
        'order_number').sum().rename({'PICK_WEIGHT': 'PICK_WEIGHT_SUM'})
    df_pick_weight.rename(columns={"PICK_WEIGHT": "PICK_WEIGHT_SUM_IN_KG"}, inplace=True)
    df_pick_weight = df_pick_weight / 1000

    df = pd.concat([df, df_pick_weight], axis=1)

    df_ambient_only = DF[['order_number', 'TEMPERATURE_CLASS']].groupby(
        'order_number').apply(lambda df: 1 if {"AMBIENT"} == set(df['TEMPERATURE_CLASS']) else 0)
    df_ambient_only.name = "TEMPERATURE_CLASS_AMBIENT_ONLY"
    
    df = pd.concat([df, df_ambient_only], axis=1)

    df.to_csv('glm_data.csv')


def checking_normal_orders():
    """
    The function calculates and prints the percentage of orders
    that needed a non-normal order pick for the goods.
    """
    global DF
    print(DF['PICK_TYPE'].value_counts())
    total_number_of_orders = len(DF['order_number'].unique())
    non_normal_orders = []
    for order_number in DF['order_number'].unique():
        df_pick_type = DF[['PICK_TYPE']][DF['order_number'] == order_number]
        if len(df_pick_type['PICK_TYPE'].unique()) > 1:
            non_normal_orders.append(order_number)
    number_of_non_normal_orders = len(non_normal_orders) / total_number_of_orders
    print(f"The percentage of non-normal orders from the total number of orders: {number_of_non_normal_orders}")

    # create pie chart
    data = [number_of_non_normal_orders, 1 - number_of_non_normal_orders]
    colors = sns.color_palette('pastel')[0:5]
    plt.pie(data, labels=['Non Normal Orders', 'Normal Orders'], colors=colors, autopct='%.0f%%')
    plt.show()


def checking_bulk_orders():
    """
    Calculate the percentage number of partially completed bulk orders.
    """
    global DF
    df_bulk_orders = DF[DF['PICK_TYPE'] == 'BULK']
    print(df_bulk_orders[['ORDERED_QTY', 'QTY']])
    number_of_partially_completed_bulks = sum(df_bulk_orders['ORDERED_QTY'] != df_bulk_orders['QTY'])
    percentage_of_partially_completed_bulks = number_of_partially_completed_bulks/df_bulk_orders.shape[0]
    print(f"Percentage of partially completed bulk orders: "
          f"{percentage_of_partially_completed_bulks}")

    # create pie chart
    data = [percentage_of_partially_completed_bulks, 1 - percentage_of_partially_completed_bulks]
    colors = sns.color_palette('pastel')[2:5]
    plt.pie(data, labels=['Partially Completed',
                          'Fully Completed'], colors=colors, autopct='%.0f%%')
    plt.title("Bulk Orders")
    plt.show()


def read_df():
    """
    This functions loads in the data and assigns the correct data type where appropriate.
    Categorical data are categorical. Numbers are integers and time as datetime.time core python objects.
    String are shown withe datatype as Object.
    All data preparation should be done here.
    """
    global DF, df_definitions
    DF = pd.read_excel('sainsburys_online_operations_test_4_.xlsx', sheet_name='Picking Data')
    df_definitions = pd.read_excel('sainsburys_online_operations_test_4_.xlsx', sheet_name='Definitions')
    for categorical_column in df_definitions['Column'][df_definitions['MathematicalType']=='categorical']:
        cat_type = CategoricalDtype(categories=DF[categorical_column].unique(),
                                    ordered=False)
        DF[categorical_column] = DF[categorical_column].astype(cat_type)
    DF['event_hour'] = [v.hour for v in DF['event_time'].values]


def main():
    global DF, df_definitions
    read_df()
    run_general_data_analysis()
    return DF, df_definitions


if __name__ == '__main__':
    main()
